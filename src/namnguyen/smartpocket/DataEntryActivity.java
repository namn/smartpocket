package namnguyen.smartpocket;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.Toast;

public abstract class DataEntryActivity extends Activity implements
		OnDateSetListener, OnClickListener {

	static final int DATE_PICKER_DIALOG_ID = 0;

	protected RecordView view;

	protected void initializeView() {
		view = new RecordView(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			initializeView();
		} catch (RuntimeException e) {
			this.finish();
			return;
		}
		setContentView(view);

		view.dateField.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog(DATE_PICKER_DIALOG_ID);
			}

		});

		view.expenseButton.setOnClickListener(this);
		view.incomeButton.setOnClickListener(this);
	}

	@Override
	public void onDateSet(DatePicker picker, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, monthOfYear, dayOfMonth);
		Date date = cal.getTime();
		view.dateField.setText(DateFormat.getDateInstance().format(date));
	}

	@Override
	public void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case DATE_PICKER_DIALOG_ID:
			Calendar date = Calendar.getInstance();
			try {
				date.setTime(DateFormat.getDateInstance().parse(
						view.dateField.getText().toString()));
			} catch (Exception e) {
				// ignored
			}
			((DatePickerDialog) dialog).updateDate(date.get(Calendar.YEAR),
					date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH));
		}
	}

	@Override
	public Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_DIALOG_ID:
			// this dialog will be updated in onPrepareDialog
			return new DatePickerDialog(this, this, 2000, 1, 1);
		}
		return null;
	}

	@Override
	public void onClick(View v) {
		ExpenseRecord record;
		try {
			record = view.getRecord();
		} catch (DataEntryException e) {
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
			return;
		}

		record.sign = (v == view.expenseButton) ? -1 : 1;

		this.save(record);

		this.afterSave();
	}

	protected abstract void afterSave();

	public void save(ExpenseRecord record) {
		ContentValues cv = new ContentValues();
		cv.put("_when", Util.toTimestamp(record.when));
		cv.put("description", record.description);
		cv.put("amount", record.amount);
		cv.put("category", record.category);
		cv.put("sign", record.sign);

		ExpenseDatabase db = ExpenseDatabase.getInstance(this);
		SQLiteDatabase conn = db.getWritableDatabase();
		if (record.id == null) {
			conn.insert(ExpenseDatabase.TABLE_NAME, null, cv);
		} else {
			conn.update(ExpenseDatabase.TABLE_NAME, cv, "id = ?",
					new String[] { Integer.toString(record.id) });
		}
		conn.close();
	}

}
