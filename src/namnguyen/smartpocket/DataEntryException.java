package namnguyen.smartpocket;

@SuppressWarnings("serial")
public class DataEntryException extends Exception {

	public DataEntryException(String string) {
		super(string);
	}

}
