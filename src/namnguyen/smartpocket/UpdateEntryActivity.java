package namnguyen.smartpocket;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class UpdateEntryActivity extends DataEntryActivity {

	@Override
	protected void initializeView() {
		int id = this.getIntent().getExtras().getInt("id");
		ExpenseDatabase db = ExpenseDatabase.getInstance(this);
		SQLiteDatabase conn = db.getReadableDatabase();
		Cursor cursor = conn.query(ExpenseDatabase.TABLE_NAME, new String[] {
				"id", "_when", "description", "amount", "category" },
				"id = ?", new String[] { Integer.toString(id) }, null, null,
				null, null);
		if (!cursor.moveToFirst()) {
			Toast.makeText(this, "Invalid record ID", Toast.LENGTH_SHORT)
					.show();
			throw new RuntimeException("Invalid record ID");
		}
		ExpenseRecord record = new ExpenseRecord();
		record.id = cursor.getInt(0);
		record.when = Util.fromTimestamp(cursor.getInt(1));
		record.description = cursor.getString(2);
		record.amount = cursor.getDouble(3);
		record.category = cursor.getString(4);
		cursor.close();
		db.close();
		view = new RecordView(this, record);
	}

	@Override
	protected void afterSave() {
		this.finish();
	}

}
