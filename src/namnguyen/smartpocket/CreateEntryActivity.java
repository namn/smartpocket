package namnguyen.smartpocket;


public class CreateEntryActivity extends DataEntryActivity {

	@Override
	protected void afterSave() {
		view.setRecord(new ExpenseRecord());
	}

}
