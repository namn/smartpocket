package namnguyen.smartpocket;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class RecordView extends LinearLayout implements OnClickListener,
		OnEditorActionListener {

	static final HashMap<String, String> TOKEN_TO_CATEGORY = new HashMap<String, String>();
	static {
		TOKEN_TO_CATEGORY.put("car", "Transportation");
		TOKEN_TO_CATEGORY.put("train", "Transportation");
		TOKEN_TO_CATEGORY.put("transport", "Transportation");
		TOKEN_TO_CATEGORY.put("bart", "Transportation");
		TOKEN_TO_CATEGORY.put("caltrain", "Transportation");
		TOKEN_TO_CATEGORY.put("flight", "Transportation");
		TOKEN_TO_CATEGORY.put("plane", "Transportation");
		TOKEN_TO_CATEGORY.put("cruise", "Transportation");
		TOKEN_TO_CATEGORY.put("gas", "Transportation");
		TOKEN_TO_CATEGORY.put("gasoline", "Transportation");
		TOKEN_TO_CATEGORY.put("salary", "Earning");
		TOKEN_TO_CATEGORY.put("wage", "Earning");
		TOKEN_TO_CATEGORY.put("breakfast", "Food (eat out)");
		TOKEN_TO_CATEGORY.put("lunch", "Food (eat out)");
		TOKEN_TO_CATEGORY.put("dinner", "Food (eat out)");
		TOKEN_TO_CATEGORY.put("eat", "Food (eat out)");
		TOKEN_TO_CATEGORY.put("restaurant", "Food (eat out)");
		TOKEN_TO_CATEGORY.put("pants", "Clothing");
		TOKEN_TO_CATEGORY.put("shirt", "Clothing");
		TOKEN_TO_CATEGORY.put("jacket", "Clothing");
		TOKEN_TO_CATEGORY.put("shoes", "Clothing");
		TOKEN_TO_CATEGORY.put("hat", "Clothing");
		TOKEN_TO_CATEGORY.put("power", "Bill");
		TOKEN_TO_CATEGORY.put("water", "Bill");
		TOKEN_TO_CATEGORY.put("utility", "Bill");
		TOKEN_TO_CATEGORY.put("phone", "Bill");
		TOKEN_TO_CATEGORY.put("cell", "Bill");
		TOKEN_TO_CATEGORY.put("cable", "Bill");
		TOKEN_TO_CATEGORY.put("internet", "Bill");
		TOKEN_TO_CATEGORY.put("mobile", "Bill");
		TOKEN_TO_CATEGORY.put("landline", "Bill");
		TOKEN_TO_CATEGORY.put("electricity", "Bill");
		TOKEN_TO_CATEGORY.put("garbage", "Bill");
		TOKEN_TO_CATEGORY.put("owe", "Debt");
		TOKEN_TO_CATEGORY.put("lend", "Debt");
		TOKEN_TO_CATEGORY.put("borrow", "Debt");
		TOKEN_TO_CATEGORY.put("show", "Entertainment");
		TOKEN_TO_CATEGORY.put("music", "Entertainment");
		TOKEN_TO_CATEGORY.put("movie", "Entertainment");
		TOKEN_TO_CATEGORY.put("theater", "Entertainment");
		TOKEN_TO_CATEGORY.put("concert", "Entertainment");
		TOKEN_TO_CATEGORY.put("rent", "Housing");
		TOKEN_TO_CATEGORY.put("mortgage", "Housing");
	}

	public TextView dateField, descriptionField, amountField;
	public Button incomeButton, expenseButton;
	public Spinner categoryField;
	public Integer id;

	public RecordView(Context ctx) {
		this(ctx, new ExpenseRecord());
	}

	public RecordView(Context ctx, ExpenseRecord record) {
		super(ctx);
		initialize();
		setRecord(record);
	}

	public void setRecord(ExpenseRecord record) {

		this.id = record.id;

		dateField.setText(DateFormat.getDateInstance().format(
				record.when.getTime()));
		descriptionField.setText(record.description);
		if (record.amount == null) {
			amountField.setText("");
		} else {
			amountField.setText(Double.toString(record.amount));
		}

		// find out top 3 most often used categories in last 30 days
		Calendar now = Util.currentDate();
		now.add(Calendar.DAY_OF_MONTH, -30);
		ExpenseDatabase db = ExpenseDatabase.getInstance(getContext());
		SQLiteDatabase conn = db.getReadableDatabase();
		Cursor cursor = conn.rawQuery(
				"SELECT category, COUNT(*) AS cat_count FROM "
						+ ExpenseDatabase.TABLE_NAME
						+ " WHERE _when >= ? GROUP BY category "
						+ "ORDER BY cat_count DESC, category ASC " + "LIMIT 3",
				new String[] { Long.toString(Util.toTimestamp(now)) });
		int selectedIndex = 0;
		int index = 0;
		ArrayList<String> categories = new ArrayList<String>(32);
		while (cursor.moveToNext()) {
			String category = cursor.getString(0);
			if (category.equals(record.category)) {
				selectedIndex = index;
			}
			categories.add(category);
			index++;
		}
		cursor.close();
		conn.close();
		int count = categories.size();
		Resources res = getResources();
		for (String cat : res.getStringArray(R.array.categories)) {
			if (count > 0 && categories.contains(cat)) {
				count--;
				continue;
			}
			if (cat.equals(record.category)) {
				selectedIndex = index;
			}
			categories.add(cat);
			index++;
		}

		// set the category drop down view
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_item, categories);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categoryField.setAdapter(adapter);
		categoryField.setSelection(selectedIndex);
	}

	public void initialize() {
		String service = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater li = (LayoutInflater) getContext().getSystemService(
				service);
		li.inflate(R.layout.data_entry, this, true);

		ImageButton calculatorButton = (ImageButton) findViewById(R.id.calculatorButton);
		calculatorButton.setOnClickListener(this);

		categoryField = (Spinner) findViewById(R.id.categoryField);
		amountField = (TextView) findViewById(R.id.amountField);
		descriptionField = (TextView) findViewById(R.id.descriptionField);
		descriptionField.setOnEditorActionListener(this);
		dateField = (TextView) findViewById(R.id.dateField);
		expenseButton = (Button) findViewById(R.id.expenseButton);
		incomeButton = (Button) findViewById(R.id.incomeButton);
	}

	@Override
	public void onClick(View v) {
		double value = 0;
		String expr = amountField.getText().toString().trim();
		if (expr.length() == 0) {
			// ignored
			return;
		}
		ExpressionEvaluator eval = new ExpressionEvaluator();
		try {
			value = eval.evaluate(expr);
		} catch (ParserException e) {
			Toast.makeText(getContext(), "Invalid expression",
					Toast.LENGTH_SHORT).show();
			return;
		}
		amountField.setText(Double.toString(value));
	}

	public ExpenseRecord getRecord() throws DataEntryException {
		Date date;
		try {
			date = DateFormat.getDateInstance().parse(
					dateField.getText().toString());
		} catch (ParseException e) {
			throw new DataEntryException("Invalid date");
		}
		
		String description = descriptionField.getText().toString().trim();
		if (description.length() == 0) {
			throw new DataEntryException("Missing description");
		}

		double amount;

		try {
			amount = Double.parseDouble(amountField.getText().toString());
		} catch (NumberFormatException e) {
			throw new DataEntryException("Invalid amount");
		}

		if (amount < 0) {
			throw new DataEntryException("Amount must be non-negative");
		}

		ExpenseRecord ret = new ExpenseRecord();
		ret.id = id;
		ret.description = description;
		ret.amount = amount;
		ret.when = Calendar.getInstance();
		ret.when.setTime(date);
		ret.category = categoryField.getSelectedItem().toString();
		return ret;
	}

	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		String desc = descriptionField.getText().toString();
		StringTokenizer tokenizer = new StringTokenizer(desc);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken().toLowerCase(Locale.getDefault());
			if (TOKEN_TO_CATEGORY.containsKey(token)) {
				String category = TOKEN_TO_CATEGORY.get(token);
				int i = 0;
				for (; i < categoryField.getCount(); i++) {
					if (category.equals(categoryField.getItemAtPosition(i))) {
						break;
					}
				}
				categoryField.setSelection(i);
				break;
			}
		}
		return false;
	}

}
