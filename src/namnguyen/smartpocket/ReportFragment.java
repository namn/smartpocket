package namnguyen.smartpocket;

import java.util.Calendar;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.commonsware.cwac.loaderex.acl.AbstractCursorLoader;


class ReportQueryParameters {
	public Calendar fromDate;
	public Calendar toDate;

	ReportQueryParameters() {
		fromDate = Util.currentMonth();
		toDate = Util.currentDate();
	}
}

final class ReportCursorLoader extends AbstractCursorLoader {

	private ExpenseDatabase db;
	private ReportQueryParameters params;
	private double income;
	private double expense;

	ReportCursorLoader(Context ctx, ReportQueryParameters params) {
		super(ctx);
		db = ExpenseDatabase.getInstance(ctx);
		this.params = params;
	}

	protected Cursor buildCursor() {
		Cursor cursor = db.getReadableDatabase().query(
				ExpenseDatabase.TABLE_NAME,
				new String[] { "id AS _id",
						"STRFTIME('%m-%d', _when, 'unixepoch') AS _when",
						"category", "description", "amount*sign AS _amount" },
				"_when >= ? AND _when < ?",
				new String[] {
						Long.toString(Util.toTimestamp(params.fromDate)),
						Long.toString(Util.toTimestamp(params.toDate)) }, null,
				null, "_when DESC, description ASC");

		while (cursor.moveToNext()) {
			double amount = cursor.getDouble(4);
			if (amount < 0) {
				expense -= amount;
			} else {
				income += amount;
			}
		}

		cursor.moveToFirst();

		return cursor;
	}

	public double getIncome() {
		return income;
	}

	public double getExpense() {
		return expense;
	}
}

public class ReportFragment extends ListFragment implements
		LoaderManager.LoaderCallbacks<Cursor> {

	private SimpleCursorAdapter report;
	private ReportQueryParameters queryParams;

	private static final int REPORT_LOADER = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
		return inflater.inflate(R.layout.report_fragment, container, false);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		queryParams = new ReportQueryParameters();
		getLoaderManager().initLoader(REPORT_LOADER, null, this);
		report = new SimpleCursorAdapter(getActivity(), R.layout.report_entry,
				null, new String[] { "_when", "category", "description",
						"_amount" }, new int[] { R.id.date, R.id.category,
						R.id.description, R.id.amount },
				CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		setListAdapter(report);
	}
	
	public void reload(Calendar reportMonth) {
		queryParams.fromDate = reportMonth;
		queryParams.toDate = (Calendar) reportMonth.clone();
		queryParams.toDate.add(Calendar.MONTH, 1);
		getLoaderManager().restartLoader(REPORT_LOADER, null,  this);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent updateIntent = new Intent(getActivity(),
				UpdateEntryActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("id", (int) id);
		updateIntent.putExtras(bundle);
		startActivity(updateIntent);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int loaderId, Bundle arg1) {
		return new ReportCursorLoader(getActivity(), this.queryParams);
	}

	private void setSums(double income, double expense) {
		TextView tv = (TextView) getView().findViewById(R.id.sumExpenseField);
		tv.setText(Double.toString(expense));

		tv = (TextView) getView().findViewById(R.id.sumIncomeField);
		tv.setText(Double.toString(income));
	}
	
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		((SimpleCursorAdapter) getListAdapter()).swapCursor(cursor);
		ReportCursorLoader rcl = (ReportCursorLoader) loader;
		setSums(rcl.getIncome(), rcl.getExpense());
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		((SimpleCursorAdapter) getListAdapter()).swapCursor(null);
		ReportCursorLoader rcl = (ReportCursorLoader) loader;
		setSums(rcl.getIncome(), rcl.getExpense());
	}

}
