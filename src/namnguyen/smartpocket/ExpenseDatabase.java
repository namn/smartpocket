package namnguyen.smartpocket;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ExpenseDatabase extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "smartpocket";
	public static final String TABLE_NAME = "income_expense";

	private static ExpenseDatabase instance;
	
	private ExpenseDatabase(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	public static synchronized ExpenseDatabase getInstance(Context ctx) {
		if (instance == null) {
			instance = new ExpenseDatabase(ctx.getApplicationContext());
		}
		return instance;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS income_expense");
		db.execSQL("CREATE TABLE income_expense (id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "_when INTEGER NOT NULL, description TEXT NOT NULL, amount REAL NOT NULL, "
				+ "category TEXT NOT NULL, sign INTEGER NOT NULL)");
		db.execSQL("CREATE INDEX when_idx ON income_expense(_when)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreate(db);
	}

}
