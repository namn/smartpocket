package namnguyen.smartpocket;

import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * An activity that generates expense/income report for a specified period.
 */
public class ReportActivity extends FragmentActivity implements OnClickListener {
	private Calendar reportDate;
	private TextView reportDateField;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.report);
		
		reportDate = Util.currentMonth();
		Button button = (Button) findViewById(R.id.previousButton);
		button.setOnClickListener(this);

		button = (Button) findViewById(R.id.nextButton);
		button.setOnClickListener(this);

		reportDateField = (TextView) findViewById(R.id.reportDateField);
		reportDateField.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int arg1,
					KeyEvent arg2) {
				try {
					reportDate = Util.getYearMonth(textView);
				} catch (NumberFormatException e) {
					return false;
				}
				refresh();
				return false;
			}

		});
		
		refresh();
	}

	private void refresh() {
		TextView tv = (TextView) findViewById(R.id.reportDateField);
		Util.setYearMonth(tv, reportDate);

		Calendar nextMonth = (Calendar) reportDate.clone();
		nextMonth.add(Calendar.MONTH, 1);

		((ReportFragment) getSupportFragmentManager()
        	.findFragmentById(R.id.reportFragment)).reload(reportDate);
	}

	@Override
	public void onClick(View v) {
		int offset = 1;
		if (v.getId() == R.id.previousButton) {
			offset = -1;
		}
		reportDate.add(Calendar.MONTH, offset);
		refresh();
	}

	@Override
	public void onResume() {
		super.onResume();
		refresh();
	}
}
