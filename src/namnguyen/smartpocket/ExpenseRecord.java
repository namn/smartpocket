package namnguyen.smartpocket;

import java.util.Calendar;

public class ExpenseRecord {

	public Integer id;
	public Calendar when;
	public Double amount;
	public String category;
	public String description;
	public int sign;
	
	public ExpenseRecord() {
		this.id = null;
		this.when = Util.currentDate();
		this.category = null;
		this.description = "";
		this.amount = null;
	}

}
