package namnguyen.smartpocket;

import java.util.Calendar;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.commonsware.cwac.loaderex.acl.AbstractCursorLoader;

final class ChartQueryParameters {
	public Calendar fromDate;
	public Calendar toDate;
	public String sign;

	public ChartQueryParameters() {
		sign = "1";
		fromDate = Util.currentMonth();
		toDate = (Calendar) fromDate.clone();
		toDate.add(Calendar.MONTH, 1);
	}
}

final class ChartCursorLoader extends AbstractCursorLoader {

	private ExpenseDatabase db;
	private ChartQueryParameters params;

	public ChartCursorLoader(Context ctx, ChartQueryParameters params) {
		super(ctx);
		db = ExpenseDatabase.getInstance(ctx);
		this.params = params;
	}

	protected Cursor buildCursor() {
		SQLiteDatabase conn = db.getReadableDatabase();
		Cursor cursor = conn.rawQuery(
				"SELECT category, SUM(amount) AS total_sum FROM "
						+ ExpenseDatabase.TABLE_NAME
						+ " WHERE sign = ? AND _when >= ? AND _when < ? "
						+ "GROUP BY category ORDER BY total_sum DESC",
				new String[] { params.sign,
						Long.toString(Util.toTimestamp(params.fromDate)),
						Long.toString(Util.toTimestamp(params.toDate)) });
		return cursor;
	}
}

public class ChartFragment extends Fragment implements
		LoaderManager.LoaderCallbacks<Cursor> {

	private static final int[] COLORS = new int[] { Color.BLUE, Color.CYAN,
			Color.GRAY, Color.GREEN, Color.MAGENTA, Color.RED, Color.YELLOW,
			Color.BLACK, Color.WHITE };

	private ChartQueryParameters queryParams;

	private static final int CHART_LOADER = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
		View r = new LinearLayout(getActivity());
		r.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		return r;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		queryParams = new ChartQueryParameters();
		getLoaderManager().initLoader(CHART_LOADER, null, this);
	}

	public void reload(Calendar fromDate, Calendar toDate, String sign) {
		queryParams.fromDate = fromDate;
		queryParams.toDate = toDate;
		queryParams.sign = sign;
		getLoaderManager().restartLoader(CHART_LOADER, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int loaderId, Bundle bundle) {
		return new ChartCursorLoader(getActivity(), queryParams);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		// from here down is to add chart elements
		DefaultRenderer renderer = new DefaultRenderer();
		renderer.setApplyBackgroundColor(true);
		renderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
		renderer.setChartTitleTextSize(0);
		renderer.setLabelsTextSize(10);
		renderer.setLegendTextSize(10);
		renderer.setStartAngle(270);
		renderer.setZoomButtonsVisible(false);
		renderer.setPanEnabled(false);

		CategorySeries series = new CategorySeries("ignored");

		while (cursor.moveToNext()) {
			String cat = cursor.getString(0);
			double sum = cursor.getDouble(1);
			series.add(cat, sum);
			SimpleSeriesRenderer ssr = new SimpleSeriesRenderer();
			ssr.setDisplayChartValues(false);
			ssr.setChartValuesTextSize(14);
			ssr.setColor(COLORS[series.getItemCount() % COLORS.length]);
			renderer.addSeriesRenderer(ssr);
		}

		View chartView;
		if (series.getItemCount() < 1) {
			chartView = new TextView(getActivity());
			((TextView) chartView).setText("No data");
		} else {
			chartView = ChartFactory.getPieChartView(getActivity(), series,
					renderer);
			((GraphicalView) chartView).repaint();
		}

		// clear out the view
		ViewGroup chartArea = (ViewGroup) getView();
		chartArea.removeAllViews();

		// add a new chart or No data text
		chartArea.addView(chartView, new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		LinearLayout chartArea = (LinearLayout) getView();
		chartArea.removeAllViews();

		TextView noData = new TextView(getActivity());
		noData.setText("No data");

		chartArea.addView(noData, new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
	}
}
