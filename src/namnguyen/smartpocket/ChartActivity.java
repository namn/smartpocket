package namnguyen.smartpocket;

import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

/**
 * A charting view.
 * 
 * This would display pie and bar charts of the expense/income in a selected
 * period.
 */
public class ChartActivity extends FragmentActivity implements
		OnEditorActionListener {

	private Spinner chartType;
	private TextView fromDate, toDate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chart);
		fromDate = (TextView) findViewById(R.id.fromDateField);
		toDate = (TextView) findViewById(R.id.toDateField);
		fromDate.setOnEditorActionListener(this);
		toDate.setOnEditorActionListener(this);
		Calendar now = Calendar.getInstance();
		Util.setYearMonth(toDate, now);
		Util.setYearMonth(fromDate, now);
		chartType = (Spinner) findViewById(R.id.chartTypeSpinner);
		chartType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				refresh();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				refresh();
			}

		});

		((Button) findViewById(R.id.refreshButton))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						refresh();
					}

				});
	}

	public void refresh() {
		String sign = "-1"; // expense
		if (chartType.getSelectedItemPosition() == 1) {
			// income
			sign = "1";
		}

		Calendar from = null, to = null;
		try {
			from = Util.getYearMonth(fromDate);
			to = Util.getYearMonth(toDate);
		} catch (NumberFormatException e) {
			Toast.makeText(this, "Invalid year/month value", Toast.LENGTH_SHORT)
					.show();
			return;
		}
		if (from.compareTo(to) > 0) {
			Toast.makeText(this, "Begin date must not be later than end date!",
					Toast.LENGTH_SHORT).show();
			return;
		}
		to.add(Calendar.MONTH, 1);

		((ChartFragment) getSupportFragmentManager().findFragmentById(
				R.id.chartFragment)).reload(from, to, sign);
	}

	@Override
	public void onResume() {
		super.onResume();
		refresh();
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		refresh();
		return false;
	}
}
