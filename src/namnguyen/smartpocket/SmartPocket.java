package namnguyen.smartpocket;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

/**
 * Main application class.
 * 
 * This class loads up all tab widgets and does nothing else.
 * 
 */
public class SmartPocket extends TabActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Resources res = getResources();
		TabHost tabHost = getTabHost();

		/* Data Entry */
		TabSpec dataEntry = tabHost.newTabSpec("Data Entry");
		Intent dataEntryIntent = new Intent().setClass(this,
				CreateEntryActivity.class);
		dataEntry.setIndicator("Data Entry",
				res.getDrawable(R.drawable.ic_editor));
		dataEntry.setContent(dataEntryIntent);
		tabHost.addTab(dataEntry);

		/* Report */
		TabSpec report = tabHost.newTabSpec("Report");
		Intent reportIntent = new Intent().setClass(this, ReportActivity.class);
		report.setIndicator("Report", res.getDrawable(R.drawable.ic_report));
		report.setContent(reportIntent);
		tabHost.addTab(report);

		/* Chart */
		TabSpec chart = tabHost.newTabSpec("Chart");
		Intent chartIntent = new Intent().setClass(this, ChartActivity.class);
		chart.setIndicator("Chart", res.getDrawable(R.drawable.ic_chart));
		chart.setContent(chartIntent);
		tabHost.addTab(chart);
	}

}