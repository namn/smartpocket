package namnguyen.smartpocket;

import java.util.Calendar;
import java.util.Date;

import android.widget.TextView;

/**
 * Utility class. This class provides commonly used functions.
 */
public class Util {

	/**
	 * Set a TextView object's text to the supplied year/month string.
	 * 
	 * @param dateField
	 *            the TextView object to set
	 * @param date
	 *            the date
	 */
	public static void setYearMonth(TextView dateField, Calendar date) {
		dateField.setText(String.format("%04d/%02d", date.get(Calendar.YEAR),
				date.get(Calendar.MONTH) + 1));
	}

	/**
	 * Return a Calendar object at the beginning of the current year.
	 * 
	 * @return a Calendar object whose year is set to current year, and other
	 *         fields to zero, except DAY_OF_MONTH to one
	 */
	public static Calendar currentYear() {
		Calendar cal = currentMonth();
		cal.set(Calendar.MONTH, 0);
		return cal;
	}

	/**
	 * Return a Calendar object at the beginning of the current month.
	 * 
	 * @return a Calendar object whose year is set to current year, month to the
	 *         current month, and other fields to zero, except DAY_OF_MONTH to
	 *         one
	 */
	public static Calendar currentMonth() {
		Calendar cal = currentDate();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal;
	}

	/**
	 * Return a Calendar object at the beginning of today.
	 * 
	 * @return a Calendar object at the beginning of today
	 */
	public static Calendar currentDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	/**
	 * Retrieve the year and month from a TextView.
	 * 
	 * @param dateField
	 *            the TextView object to get the text from
	 * @return a Calendar object at the beginning of the year or of the month in
	 *         the year
	 * @throws NumberFormatException
	 */
	public static Calendar getYearMonth(TextView dateField)
			throws NumberFormatException {
		String value = dateField.getText().toString().trim();
		if (value.length() == 0) {
			throw new NumberFormatException("Empty date string");
		}
		String fields[] = value.split("/");
		Calendar cal = currentYear();
		cal.set(Calendar.YEAR, 0);
		switch (fields.length) {
		case 2:
			int month = Integer.valueOf(fields[1]);
			cal.set(Calendar.MONTH, month - 1);
			// fall through
		case 1:
			int year = Integer.valueOf(fields[0]);
			cal.set(Calendar.YEAR, year);
			break;
		default:
			throw new NumberFormatException("Only support year/month format");
		}
		return cal;
	}

	/**
	 * Return seconds from UNIX epoch.
	 * 
	 * @param cal
	 *            the Calendar object
	 * @return seconds from UNIX epoch
	 */
	public static long toTimestamp(Calendar cal) {
		return toTimestamp(cal.getTime());
	}

	/**
	 * Return seconds from UNIX epoch.
	 * 
	 * @param date
	 *            the Date object
	 * @return seconds from UNIX epoch
	 */
	public static long toTimestamp(Date date) {
		return date.getTime() / 1000;
	}

	/**
	 * Return a Calendar object corresponding to a timestamp.
	 * 
	 * @param timestamp
	 *            the UNIX timestamp
	 * @return a Calendar object
	 */
	public static Calendar fromTimestamp(long timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp * 1000);
		return cal;
	}
}
